<?php
declare(strict_types=1);

/*
 * Note: Works only for Indexed array as question has specified, NOT compatible for Associative array
 */
function customArrayReverse(array $inputArray): array {
    $outputArray = [];
    $arraySize   = count($inputArray);

    for ($i = 1; $i <= $arraySize; $i++) {
        $outputArray[$i - 1] = $inputArray[$arraySize - $i];
    }

    return $outputArray;
}

/*
 // Quick and dirty test..
var_dump(customArrayReverse(['Apple', 'Banana', 'Orange', 'Coconut']));
var_dump(customArrayReverse(['zero', 'one', 'two', 'three']));
var_dump(customArrayReverse([3, 2, 1, 0]));
var_dump(customArrayReverse([]));
*/

