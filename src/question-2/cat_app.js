const cat_app = Vue.createApp({
    data() {
        return {
            loading: true,
            cat_data: null,
            modal: {title: "Loading .. ", description: "loading.. ", image: {url: "./images/loading-bar.gif"}},
        }
    },
    methods: {
        getCats(url) {
            httpRequest(url).then(result => this.cat_data = result);
        }
        , getRandomImage(values) {
            this.modal.image.url = './images/loading-bar.gif';
            httpRequest('https://api.thecatapi.com/v1/images/search').then(result => this.modal.image = result[0]);

            this.modal.title = 'Cat Length ' + values.length;
            this.modal.description = values.fact;
        }
    }
    ,
    mounted() {

        document.addEventListener('DOMContentLoaded', function () {
            var elems = document.querySelectorAll('.modal');
            var instances = M.Modal.init(elems);
        });

        httpRequest('https://catfact.ninja/facts?limit=9').then(result => this.cat_data = result)
        httpRequest('https://api.thecatapi.com/v1/images/search').then(result => this.cat_image = result)

        ;
    }
})

cat_app.mount('#cat_app');

async function httpRequest(url, verb, data, dataType) {

    if (!url) {
        alert("Please specify a URL"); // quick and dirty way
        return;
    }
    if (!verb) verb = 'get';

    // allowed verbs
    let allowedVerbs = ['get', 'post']; //  PUT, PATCH, and DELETE.
    httpVerb = allowedVerbs.includes(verb.toLowerCase()) ? verb : 'get';

    return await axios({
        method: httpVerb,
        url: url,
    })

        .then(async function (response) {
            return await response.data;

        })
        .catch(
            function (error) {
                console.warn('An Error Occurred');
                console.error(error);
                return Promise.reject(error)
            }
        )
        ;
}

console.info("\n\n\n\n This is a test application and has not been optimized for PROD usage.\n\n\n\n  \nThanks Singh");
