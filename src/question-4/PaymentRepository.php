<?php

namespace App\Repositories;

use App\Models\Payment;

class PaymentRepository
{
    public function getAll(array $reqOptions): ?array
    {

        //    $payments = $art->products->offset($offset*$limit)->take($limit)->get();


/*
              // THIS is ideal sample   of doing it
        $limit =  $reqOptions['limit'] ?? 20 ;
        $payments = Payment::select('id', 'other', 'required','field', 'names', 'here')
            ->where('field','value')
            ->orderBy('id', 'DESC')
            ->take($limit)->get();
        */

        //        $payments = Payment::all(); // table with roughly 1.2m records
        //  ::all() // is a very bad way of doing this is what is loading 1.2 m records will all fields present

        $return = [];
        $limit =  $reqOptions['limit'] ?? 20 ;
        $payments = Payment::select('id', 'other', 'required','field', 'names', 'here') ;
        if ($reqOptions['sortDir'] === 'desc') {
            $payments = $payments->sortByDesc('id');
        }

        $payments->take($limit)->get();

        if (!count($payments)) {
            return $return;
        }


        for ($i = 0; $i < $reqOptions['limit'] ?? 20; $i++) {
            $pmt = $payments[$i]->toArray();

            $return[] = array_merge($pmt, [
                'facility' => $payments[$i]->facility,
                'user' => $payments[$i]->user,
            ]);
        }

        return $return;
    }
}
