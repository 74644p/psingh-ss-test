<?php

declare(strict_types=1);


use App\Http\Entities\Facility;
use Illuminate\Support\Str;
use PHPUnit\Framework\TestCase;

final class FacilityTest extends TestCase {

    public function testBasicInit(): void {
        // Quick and dirty test if unit test is working..
        $facility = new Facility();
        $city     = 'Duluth';
        $facility->setCity($city);
        $this->assertEquals($city, $facility->getCity());
    }

    public function testGetRouteParams(): void {
        //NOTE this is weird.. but i am trying to write unit test only..
        // requirement is not clear for this question.. so only writing test for coverage

        $facilitySlug = new Facility();

        $state         = "California";
        $county        = "Orange";
        $city          = "La Palma";
        $location_code = "34.0522 118.2437";

        $facilitySlug->setState($state);
        $facilitySlug->setCounty($county);
        $facilitySlug->setCity($city);
        $facilitySlug->setLocationCode($location_code);

        $returnArray = $facilitySlug->getRouteParams();


        $this->assertEquals($returnArray['state'], Str::slug($state));
        $this->assertEquals($returnArray['county'], Str::slug($county."-county")); // concat county
        $this->assertEquals($returnArray['city'], Str::slug($city));
        $this->assertEquals($returnArray['location_code'], Str::slug($location_code));


    }

}
